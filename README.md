## Description

This project includes scripts to enable retransmitting data collected 
from the Myo device under Windows to another computer on the network. 

## Requirements

- [Python Bindings for Myo SDK](https://github.com/NiklasRosenstein/myo-python) [required]

## Usage

Ensure Myo Connect is running and that the Myo SDK and python bindings are installed. 

	$ python myo_transmitter.py DESTINATION_IP DESTINATION_PORT