# Copyright (c) 2015 Sasanka Nagavalli
# Based on libmyo example by Niklas Rosenstein
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import myo as libmyo; libmyo.init()

import argparse
import json
import socket
import time

class MyoTransmitter(libmyo.DeviceListener):

    def __init__(self, udp_ip, udp_port):
        super(MyoTransmitter, self).__init__()
        self.orientation = None
        self.pose = libmyo.Pose.rest
        self.locked = False
        self.rssi = None
        self.emg = None
        self.ip = udp_ip
        self.port = udp_port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def transmit(self, message):
        print message
        self.sock.sendto(json.dumps(message), (self.ip, self.port))

    def on_connect(self, myo, timestamp, firmware_version):
        myo.vibrate('short')
        myo.request_rssi()
        myo.request_battery_level()
        myo.set_stream_emg(libmyo.StreamEmg.disabled)

    def on_rssi(self, myo, timestamp, rssi):
        self.rssi = rssi

    def on_pose(self, myo, timestamp, pose):
        self.pose = pose
        message = {
            'gesture' : {
                'myo' : myo.value,
                'timestamp' : timestamp,
                'gesture' : self.pose.name
            }
        }
        self.transmit(message)

    def on_orientation_data(self, myo, timestamp, orientation):
        self.orientation = orientation
        message = { 
            'orientation' : {
                'myo' : myo.value,
                'timestamp' : timestamp,
                'quaternion_xyzw' : [
                    self.orientation.x,
                    self.orientation.y,
                    self.orientation.z,
                    self.orientation.w
                ]
            }
        }
        self.transmit(message)

    def on_accelerometor_data(self, myo, timestamp, acceleration):
        pass

    def on_gyroscope_data(self, myo, timestamp, gyroscope):
        pass

    def on_emg_data(self, myo, timestamp, emg):
        self.emg = emg
        message = {
            'emg_data' : {
                'myo' : myo.value,
                'timestamp' : timestamp,
                'data' : self.emg
            }
        }
        self.transmit(message)

    def on_unlock(self, myo, timestamp):
        self.locked = False

    def on_lock(self, myo, timestamp):
        self.locked = True

    def on_event(self, kind, event):
        pass

    def on_event_finished(self, kind, event):
        pass

    def on_pair(self, myo, timestamp, firmware_version):
        pass

    def on_unpair(self, myo, timestamp):
        pass

    def on_disconnect(self, myo, timestamp):
        pass

    def on_arm_sync(self, myo, timestamp, arm, x_direction, rotation,
                    warmup_state):
        pass

    def on_arm_unsync(self, myo, timestamp):
        pass

    def on_battery_level_received(self, myo, timestamp, level):
        pass

    def on_warmup_completed(self, myo, timestamp, warmup_result):
        pass


def main():
    parser = argparse.ArgumentParser(
        description='Retransmit Myo data to destination over UDP.',
        epilog='Example: %(prog)s 192.168.1.22 59052')
    parser.add_argument('ip', help='destination IP')
    parser.add_argument('port', type=int, help='destination port')
    args = parser.parse_args()

    print("Connecting to Myo ... Use CTRL^C to exit.")
    try:
        hub = libmyo.Hub()
    except MemoryError:
        print("Myo Hub could not be created. Make sure Myo Connect is running.")
        return

    hub.set_locking_policy(libmyo.LockingPolicy.none)
    hub.run(1000, MyoTransmitter(args.ip, args.port))

    try:
        while hub.running:
            time.sleep(0.25)
    except KeyboardInterrupt:
        print("\nQuitting ...")
    finally:
        print("Shutting down hub...")
        hub.shutdown()


if __name__ == '__main__':
    main()
